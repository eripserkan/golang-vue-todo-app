docker login -u serkanerip --password=$DOCKER_PASS
docker build --build-arg ENV=prod -t serkanerip/todo-app-frontend:latest frontend/.
docker build --build-arg ENV=development -t serkanerip/todo-app-frontend:dev frontend/.
docker push serkanerip/todo-app-frontend:latest
docker push serkanerip/todo-app-frontend:dev
docker build -t serkanerip/todo-app-backend:latest web-service/.
docker push serkanerip/todo-app-backend:latest