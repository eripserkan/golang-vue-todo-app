## Table of contents
- [Table of contents](#table-of-contents)
- [General Info](#general-info)
- [Features](#features)
- [Technologies](#technologies)
- [Architectural Decisions](#architectural-decisions)
  - [Frontend](#frontend)
  - [Backend](#backend)
  - [Database](#database)
  - [Deployment](#deployment)
- [Installation](#installation)
- [Frontend Development](#frontend-development)
- [Backend Development](#backend-development)
- [Run With Docker](#run-with-docker)
- [Web Api](#web-api)
  - [Get list of todos](#get-list-of-todos)
    - [Request](#request)
    - [Response](#response)
  - [Create a new todo](#create-a-new-todo)
    - [Request](#request-1)
    - [Response](#response-1)
- [License](#license)

## General Info
This is a basic todo app that you can create and list todos.

App is live on [http://assignment.serkanerip.com/.](http://assignment.serkanerip.com/)

## Features

* Create Todo
* List created Todos

## Technologies

* Golang
    * Go internal testing lib. for unit/integration testing
    * Go-pact for contract driven producer testing
    * Gin web framework for request handling
* Vue
    * Cypress for e2e, acceptance testing
    * Jest for unit/integration testing
    * Pact for contract driven consumer testing
* Docker
* Docker Compose

## Architectural Decisions

### Frontend

* I applyed the Domain Driven Design principles when planning application directory structure.
* Different directories for all the different modules (=domains)
* Structuring the application that way makes it easier to extract parts of it into separate Microfrontends later. It also makes it easier to find stuff if our application grows to thousands of files and components.

### Backend

* The value in Go's simplicity.  I try to keep it simple and readable.
* Same as frontend different directories for all the different modules(=domains).
* Business logics are written in todo service.


### Database
* In memory database used.

### Deployment

* There is two vm on Azure Cloud Provider, one for production and one for running UI acceptance and cdc tests.
![azure](./images/azure.png)
* CI/CD pipeline runs on gitlab runners.
![pipeline](./images/pipeline.png)
    1. Build frontend and backend projects
    2. Run tests
    3. Build docker images for each from Dockerfiles, then push images to docker hub image registry
    4. Deploy app to test vm, run cdc and ui acceptance tests. (Docker-compose)
    5. Deploy to production (Docker-compose)


## Installation

```bash
git clone git@gitlab.com:eripserkan/golang-vue-todo-app.git
```

## Frontend Development
```bash
cd frontend

yarn install // installing dependencies
yarn serve

// testing commands
yarn test:unit
yarn test:pact
yarn test:integration
yarn test:e2e

// build commands
yarn build:development
yarn build:prod
```

## Backend Development

```bash
cd web-service

go get -v // installing dependencies
go run . // start app

go test -v ./... // run tests
go build . // build

```

## Run With Docker
If you dont know how to install docker read the [docs.](https://docs.docker.com/engine/install/)

```
docker-compose up
```

## Web Api

### Get list of todos

#### Request
[**GET**] /api/v1/todos
```bash
$ curl -H "Content-type: application/json" localhost:3000/api/v1/todos
```
#### Response
```json
[
    {
        "id": 1,
        "description": "Build a CI/CD Pipeline",
        "completed_at": "0001-01-01T00:00:00Z"
    }
]
```
### Create a new todo

#### Request
[**POST**] /api/v1/todos
```bash
$ curl -H "Content-type: application/json" -X POST -d '{"description":"Build a CI/CD Pipeline"}' localhost:3000/api/v1/todos
```
#### Response
```json
{
    "id": 1,
    "description": "Build a CI/CD Pipeline",
    "completed_at": "0001-01-01T00:00:00Z"
}
```

## License

The MIT License  2021 Serkan Erip
