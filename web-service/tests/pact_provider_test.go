package tests

import (
	"fmt"
	"os"
	"path/filepath"
	"testing"

	"github.com/joho/godotenv"
	"github.com/magiconair/properties/assert"

	"github.com/pact-foundation/pact-go/v2/provider"
	"gitlab.com/eripserkan/golang-vue-todo-app/globals"
	"gitlab.com/eripserkan/golang-vue-todo-app/router"
	"gitlab.com/eripserkan/golang-vue-todo-app/todo"
	"gitlab.com/eripserkan/golang-vue-todo-app/todo/domain/entities"
	"gitlab.com/eripserkan/golang-vue-todo-app/todo/domain/services"
)

func startServer() {
	godotenv.Load()
	router := router.GetRouter()

	todo.SetupTodoRoutes(router)
	router.Run(fmt.Sprintf(":%v", globals.DEFAULT_PORT))
}

func TestProvider(t *testing.T) {
	go startServer()

	services.GetTodoService().CreateTodo(&entities.Todo{Description: "Buy some milks"})

	verifier := provider.HTTPVerifier{}

	workDir, _ := os.Getwd()
	err := verifier.VerifyProvider(t, provider.VerifyRequest{
		ProviderBaseURL: fmt.Sprintf("http://localhost:%v", globals.DEFAULT_PORT),
		PactFiles:       []string{filepath.ToSlash(fmt.Sprintf("%s/../../frontend/pacts/todo_vue_client-todo_go_web_service.json", workDir))},
	})

	assert.Equal(t, err, nil)
}
