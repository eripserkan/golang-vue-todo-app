package router

import "github.com/gin-gonic/gin"

var (
	router *gin.Engine
)

func init() {
	router = gin.New()
	router.Use(gin.Recovery())
}

func GetRouter() *gin.Engine {
	return router
}
