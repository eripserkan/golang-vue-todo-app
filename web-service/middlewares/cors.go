package middlewares

import (
	"log"
	"os"

	cors "github.com/rs/cors/wrapper/gin"
	"gitlab.com/eripserkan/golang-vue-todo-app/router"
)

type corsMiddleware struct{}

func (m corsMiddleware) Setup() {
	handler := router.GetRouter()
	log.Println("Setting up cors middleware")

	debug := os.Getenv("ENV") == "development"
	handler.Use(cors.New(cors.Options{
		AllowCredentials: true,
		AllowOriginFunc:  func(origin string) bool { return true },
		AllowedHeaders:   []string{"*"},
		AllowedMethods:   []string{"GET", "POST", "PUT", "HEAD", "OPTIONS"},
		Debug:            debug,
	}))
}
