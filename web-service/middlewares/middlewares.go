package middlewares

type IMiddleware interface {
	Setup()
}

var (
	middlewares []IMiddleware = []IMiddleware{corsMiddleware{}}
)

func SetupMiddlewares() {
	for _, middleware := range middlewares {
		middleware.Setup()
	}
}
