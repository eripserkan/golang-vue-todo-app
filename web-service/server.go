package main

import (
	"fmt"
	"os"

	"github.com/joho/godotenv"

	"gitlab.com/eripserkan/golang-vue-todo-app/globals"
	"gitlab.com/eripserkan/golang-vue-todo-app/middlewares"
	"gitlab.com/eripserkan/golang-vue-todo-app/router"
	"gitlab.com/eripserkan/golang-vue-todo-app/todo"
)

func main() {
	godotenv.Load()
	router := router.GetRouter()
	middlewares.SetupMiddlewares()
	todo.SetupTodoRoutes(router)

	var port = globals.DEFAULT_PORT
	if val, ok := os.LookupEnv("SERVER_PORT"); ok {
		port = val
	}

	router.Run(fmt.Sprintf(":%v", port))
}
