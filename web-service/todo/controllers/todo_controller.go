package controllers

import (
	"log"

	"github.com/gin-gonic/gin"
	"gitlab.com/eripserkan/golang-vue-todo-app/todo/domain/entities"
	"gitlab.com/eripserkan/golang-vue-todo-app/todo/domain/services"
)

var todoService = services.GetTodoService()

func GetAll(c *gin.Context) {
	todos, err := todoService.GetAll()
	if err != nil {
		log.Println(err)
	}
	c.JSON(200, todos)
}

func Create(c *gin.Context) {
	var todo entities.Todo
	c.BindJSON(&todo)
	err := todoService.CreateTodo(&todo)
	if err != nil {
		log.Println(err)
	}
	c.JSON(200, todo)
}
