package entities

import (
	"time"
)

type Todo struct {
	ID          uint      `json:"id"`
	Description string    `json:"description"`
	CompletedAt time.Time `json:"completed_at"`
}

func (t Todo) TableName() string {
	return "todos"
}
