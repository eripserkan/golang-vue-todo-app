package services

import (
	"gitlab.com/eripserkan/golang-vue-todo-app/todo/domain/entities"
)

type TodoService struct{}

var (
	service TodoService
	todoDB  []entities.Todo
)

func init() {
	service = TodoService{}
	todoDB = make([]entities.Todo, 0)
}

func GetTodoService() TodoService {
	return service
}

func (s TodoService) GetAll() (todos []entities.Todo, err error) {
	todos = todoDB
	return todos, nil
}

func (s TodoService) CreateTodo(todo *entities.Todo) error {
	todo.ID = uint(len(todoDB) + 1)
	todoDB = append(todoDB, *todo)
	return nil
}
