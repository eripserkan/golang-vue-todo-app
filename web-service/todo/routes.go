package todo

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/eripserkan/golang-vue-todo-app/todo/controllers"
)

func SetupTodoRoutes(gin *gin.Engine) {
	api := gin.Group("/api/v1/todos/")
	{
		api.GET("", controllers.GetAll)
		api.POST("", controllers.Create)
	}
}
