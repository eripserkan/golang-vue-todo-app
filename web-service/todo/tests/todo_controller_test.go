package tests

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/magiconair/properties/assert"
	"gitlab.com/eripserkan/golang-vue-todo-app/router"
	"gitlab.com/eripserkan/golang-vue-todo-app/todo"
	"gitlab.com/eripserkan/golang-vue-todo-app/todo/domain/entities"
	"gitlab.com/eripserkan/golang-vue-todo-app/todo/domain/services"
)

var (
	Gin *gin.Engine
)

func init() {
	gin.SetMode(gin.TestMode)
	Gin = router.GetRouter()
	todo.SetupTodoRoutes(Gin)
}

func RunRequest(req *http.Request) *httptest.ResponseRecorder {
	w := httptest.NewRecorder()
	Gin.ServeHTTP(w, req)
	return w
}

func TestGetAllTodos(t *testing.T) {
	todoService := services.GetTodoService()
	todoService.CreateTodo(&entities.Todo{Description: "Buy some milks"})

	req, _ := http.NewRequest("GET", "/api/v1/todos/", nil)
	w := RunRequest(req)

	var todos []entities.Todo
	json.Unmarshal(w.Body.Bytes(), &todos)

	assert.Equal(t, w.Code, 200)
	assert.Equal(t, len(todos), 1)
}

func TestCreateTodo(t *testing.T) {
	b, requestBody := new(bytes.Buffer), map[string]string{"description": "Buy Eggs"}

	json.NewEncoder(b).Encode(requestBody)

	req, _ := http.NewRequest("POST", "/api/v1/todos/", b)
	w := RunRequest(req)

	var todo entities.Todo
	json.Unmarshal(w.Body.Bytes(), &todo)

	assert.Equal(t, w.Code, 200)
	assert.Equal(t, todo.Description, requestBody["description"])
}
