module.exports = {
  testMatch: [
    "**/tests/unit/*.pact.[jt]s?(x)",
    "**/src/modules/**/tests/unit/*.pact.[jt]s?(x)",
  ],
};
