// eslint-disable-next-line @typescript-eslint/no-var-requires
const config = require(`./jest.${process.env.TEST_CLASS}.config`);

module.exports = {
  preset: "@vue/cli-plugin-unit-jest/presets/typescript-and-babel",
  transform: {
    "^.+\\.vue$": "vue-jest",
  },
  ...config,
};
