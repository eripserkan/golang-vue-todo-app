import { Todo } from "@/modules/todo/entities/Todo";
import * as faker from "faker";

export function createTodo(): Todo {
  return {
    id: faker.datatype.number(),
    description: faker.lorem.sentence(),
  };
}

export function createTodoList(numberOfTodo: number): Array<Todo> {
  return Array(numberOfTodo)
    .fill(0)
    .map(() => ({
      id: faker.datatype.number(),
      description: faker.lorem.sentence(),
    }));
}
