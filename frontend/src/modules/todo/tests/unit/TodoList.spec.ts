import { shallowMount } from "@vue/test-utils";
import TodoList from "@/modules/todo/components/TodoList.vue";
import { createTodoList } from "../utils/MockTodoCreator";

describe("todo/components/TodoList.vue", () => {
  it("renders todos", () => {
    // arrange
    const numberOfTodos = 2;
    const todosMockData = createTodoList(numberOfTodos);

    // act
    const wrapper = shallowMount(TodoList, {
      props: {
        todos: todosMockData,
      },
    });
    const todoElements = wrapper.findAll(".todo");

    // assert
    expect(todoElements.length).toBe(numberOfTodos);
  });
});
