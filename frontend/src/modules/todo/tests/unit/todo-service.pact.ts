import TodoService from "../../services/TodoService";
declare let process: any;

import * as path from "path";
import { Pact } from "@pact-foundation/pact";
import {
  createTodoInteraction,
  fetchAllInteraction,
  mockTodo,
} from "@/modules/todo/tests/unit/pacts/interactions";

const url = "http://localhost";
let todoService: TodoService;

const PORT = 3346;

const provider = new Pact({
  port: PORT,
  log: path.resolve(process.cwd(), "logs", "mockserver-integration.log"),
  dir: path.resolve(process.cwd(), "pacts"),
  spec: 2,
  cors: true,
  consumer: "Todo Vue Client",
  provider: "Todo Go Web Service",
  logLevel: "debug",
});

describe("Todo service", () => {
  beforeAll(async () => {
    await provider.setup();
  });

  afterAll((done) => {
    provider
      .verify()
      .then(() => provider.finalize())
      .then(() => done());
  });

  beforeAll(async () => {
    todoService = new TodoService(`${url}:${PORT}/api/v1/todos`);
    await provider.addInteraction(fetchAllInteraction);
    await provider.addInteraction(createTodoInteraction);
  });

  test("should fetch the list of todos", (done) => {
    todoService
      .fetchTodos()
      .then((todos) => {
        expect(todos.length).toEqual(1);
      })
      .catch((err) => console.log(err))
      .finally(() => done());
  });

  test("should create a todo", (done) => {
    todoService
      .create(mockTodo.description)
      .then((todo) => {
        expect(todo.description).toEqual(mockTodo.description);
      })
      .catch((err) => console.log(err))
      .finally(() => done());
  });
});
