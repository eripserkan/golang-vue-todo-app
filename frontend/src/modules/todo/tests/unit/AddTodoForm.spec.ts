import { shallowMount } from "@vue/test-utils";
import AddTodoForm from "@/modules/todo/components/AddTodoForm.vue";
import * as faker from "faker";

describe("todo/components/AddTodoForm.vue", () => {
  it("renders input", () => {
    const wrapper = shallowMount(AddTodoForm);

    expect(wrapper.find('input[type="text"]').exists()).toBe(true);
  });

  it("renders button", () => {
    const wrapper = shallowMount(AddTodoForm);

    expect(wrapper.find("button").exists()).toBe(true);
  });

  it("it should emit add-new-todo", async () => {
    const description = faker.lorem.sentence();
    const wrapper = shallowMount(AddTodoForm);
    const input = wrapper.find('input[type="text"]');
    const button = wrapper.find("button");

    await input.setValue(description);
    await button.trigger("click");

    expect(wrapper.emitted("addNewTodo")).toHaveLength(1);
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    expect(wrapper.emitted("addNewTodo")[0][0]).toBe(description);
  });
});
