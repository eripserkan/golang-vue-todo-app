import { shallowMount } from "@vue/test-utils";
import { createTodo } from "../utils/MockTodoCreator";
import TodoListItem from "@/modules/todo/components/TodoList/TodoListItem.vue";

describe("todo/components/TodoList/TodoListItem.vue", () => {
  it("renders todo", () => {
    // arrange
    const mockTodo = createTodo();

    // act
    const wrapper = shallowMount(TodoListItem, {
      props: {
        todo: mockTodo,
      },
    });

    // assert
    expect(wrapper.text()).toBe(mockTodo.description);
  });
});
