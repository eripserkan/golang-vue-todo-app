import { Interaction, Matchers } from "@pact-foundation/pact";
const { eachLike, like } = Matchers;

export const mockTodo = {
  id: 1,
  description: "Buy some milks",
  completed_at: "",
};

const FETCH_ALL_EXPECTED_BODY = eachLike(mockTodo);

export const fetchAllInteraction: Interaction = new Interaction()
  .uponReceiving("")
  .given("I have a list of todos")
  .withRequest({
    method: "GET",
    path: "/api/v1/todos/",
    headers: {
      Accept: "application/json",
    },
  })
  .willRespondWith({
    status: 200,
    body: FETCH_ALL_EXPECTED_BODY,
  });

export const createTodoInteraction: Interaction = new Interaction()
  .uponReceiving("Should create a todo")
  .given("I created todo")
  .withRequest({
    method: "POST",
    path: "/api/v1/todos/",
    headers: {
      Accept: "application/json",
    },
    body: { description: mockTodo.description },
  })
  .willRespondWith({
    status: 200,
    body: like(mockTodo),
  });
