import { Todo } from "@/modules/todo/entities/Todo";
import axios from "axios";

function createApiClient(url?: string) {
  if (!url) {
    url = process.env.VUE_APP_TODO_API_BASE_URL || "http://localhost:3000/api/v1/todos";
  }
  return axios.create({
    baseURL: url,
    withCredentials: false,
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
  });
}

export default class TodoService {
  client;

  constructor(url?: string) {
    this.client = createApiClient(url);
  }

  async fetchTodos(): Promise<Array<Todo>> {
    return await (
      await this.client.get("/")
    ).data;
  }

  async create(description: string): Promise<Todo> {
    return await (
      await this.client.post("/", { description })
    ).data;
  }
}
