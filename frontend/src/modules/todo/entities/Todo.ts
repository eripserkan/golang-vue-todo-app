export type Todo = {
  id: number;
  description: string;
  completed_at?: string;
};
