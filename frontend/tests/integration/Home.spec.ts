import { mount } from "@vue/test-utils";
import TodoList from "@/modules/todo/components/TodoList.vue";
import Home from "@/pages/Home.vue";
import AddTodoForm from "@/modules/todo/components/AddTodoForm.vue";
import TodoService from "@/modules/todo/services/TodoService";
import { createTodoList } from "@/modules/todo/tests/utils/MockTodoCreator";
import * as faker from "faker";

// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
let wrapper;

afterEach(() => {
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  wrapper.unmount();
});

describe("pages/Home.vue", () => {
  it("renders todo-list component", () => {
    // act
    wrapper = mount(Home);

    // assert
    expect(wrapper.findComponent(TodoList).exists()).toBe(true);
  });
  it("renders add-todo-form component", () => {
    // act
    wrapper = mount(Home);

    // assert
    expect(wrapper.findComponent(AddTodoForm).exists()).toBe(true);
  });
  it("gives todos coming from service to the todo-list component as prop", async () => {
    // arrange
    const mockTodos = createTodoList(5);
    const mockFetchTodos = jest.fn();
    TodoService.prototype.fetchTodos = () => mockFetchTodos();
    mockFetchTodos.mockReturnValue(Promise.resolve(mockTodos));

    // act
    wrapper = mount(Home);

    await wrapper.vm.$nextTick();
    // assert
    expect(mockFetchTodos).toHaveBeenCalledTimes(1);
    expect(wrapper.vm.todos).toStrictEqual(mockTodos);
  });
  it("should add new todo to the list", async () => {
    // arrange
    const description = faker.lorem.sentence();
    const mockCreate = jest.fn();
    const mockFetchTodos = jest.fn();
    TodoService.prototype.fetchTodos = () => mockFetchTodos();
    mockFetchTodos.mockReturnValue(Promise.resolve([]));
    TodoService.prototype.create = mockCreate;
    mockCreate.mockReturnValue(
      Promise.resolve({
        id: 1,
        description,
      })
    );

    // act
    wrapper = mount(Home);
    wrapper.findComponent(AddTodoForm).vm.$emit("addNewTodo", description);
    await wrapper.vm.$nextTick();

    // assert
    expect(mockCreate).toBeCalledWith(description);
    expect(wrapper.vm.todos.at(-1).description).toEqual(description);
  });
  it("should alert error message if add-new-todo failed", async () => {
    // arrange
    const description = faker.lorem.sentence();
    const mockCreate = jest.fn();
    jest.spyOn(window, "alert").mockImplementation(() => null);
    TodoService.prototype.create = mockCreate;
    mockCreate.mockReturnValue(Promise.reject());

    // act
    wrapper = mount(Home);
    wrapper.findComponent(AddTodoForm).vm.$emit("addNewTodo", description);
    await wrapper.vm.$nextTick();

    // assert
    expect(window.alert).toHaveBeenCalled();
  });
  it("should alert error message if fetchTodos failed", async () => {
    // arrange
    jest.spyOn(window, "alert").mockImplementation(() => null);
    const mockFetchTodos = jest.fn();
    TodoService.prototype.fetchTodos = mockFetchTodos;
    mockFetchTodos.mockReturnValue(Promise.reject());

    // act
    wrapper = mount(Home);
    await wrapper.vm.$nextTick();

    // assert
    expect(window.alert).toHaveBeenCalled();
  });
});
