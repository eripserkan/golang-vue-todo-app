describe("Todos", () => {
  it("should add new todo", () => {
    cy.visit(Cypress.env("BASE_URL"));
    cy.get(".new-todo-input").type("Buy some milks");
    cy.get(".add-todo-button").click();
    cy.wait(500);
    cy.get(".todo").should("have.length", 1);
  });
});
