module.exports = {
  testMatch: [
    "**/tests/integration/*.spec.[jt]s?(x)",
    "**/src/modules/**/tests/integration/*.spec.[jt]s?(x)",
  ],
};
