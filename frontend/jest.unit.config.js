module.exports = {
  testMatch: [
    "**/tests/unit/*.spec.[jt]s?(x)",
    "**/src/modules/**/tests/unit/*.spec.[jt]s?(x)",
  ],
};
